package com.tmtron.ex.unknown.types.jvmmodel

import javax.inject.Inject
import org.apache.commons.text.StringEscapeUtils
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.resource.FileExtensionProvider

class GenerateSyntaxHighlighting {

	@Inject private FileExtensionProvider extensionProvider
	
	def work(IFileSystemAccess fsa, String[] tokenNames) {
		val primaryExt = extensionProvider.primaryFileExtension
		val fileName = primaryExt+".xml"

		fsa.generateFile(fileName, fileContent(tokenNames))
	}
	
	def unquote(String input) {
		return input.replace("'", "")
	}
	
	def xmlEscape(String input) {
		return StringEscapeUtils.escapeXml11(input)
	}

	def keywordTag(String token) {
		val firstChar = token.unquote.charAt(0)
		if (Character.isLetter(firstChar)) {
			return 'keywords'
		} else {
			return 'keywords2'
		}
	}
	
	def String fileContent(String[] tokenNames) {
		val fileExt = extensionProvider.primaryFileExtension
		return '''
			<filetype binary="false" description="TMTron: DSL «fileExt»" name="«fileExt»">
			  <highlighting>
			    <options>
			      <option name="LINE_COMMENT" value="//" />
			      <option name="COMMENT_START" value="/*" />
			      <option name="COMMENT_END" value="*/" />
			      <option name="HEX_PREFIX" value="" />
			      <option name="NUM_POSTFIXES" value="" />
			      <option name="HAS_BRACES" value="true" />
			      <option name="HAS_BRACKETS" value="true" />
			      <option name="HAS_PARENS" value="true" />
			    </options>
			    «FOR token : tokenNames»
			    	«IF token.startsWith("'")»
			    		<«token.keywordTag» keywords="«token.unquote.xmlEscape»" ignore_case="true" />
			    	«ENDIF»
			  «ENDFOR»
			  </highlighting>
			  <extensionMap>
			    <mapping ext="«fileExt»" />
			  </extensionMap>
			</filetype>
		'''
	}
		
}
