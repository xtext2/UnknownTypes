package com.tmtron.ex.unknown.types.jvmmodel

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.xbase.compiler.JvmModelGenerator
import com.google.inject.Inject
import com.tmtron.ex.unknown.types.parser.antlr.internal.InternalUtDslParser

class UtJvmModelGenerator extends JvmModelGenerator {
	@Inject package GenerateSyntaxHighlighting generateSyntaxHighlighting

	override void doGenerate(Resource input, IFileSystemAccess fsa) {
		super.doGenerate(input, fsa)
		generateSyntaxHighlighting.work(fsa, InternalUtDslParser::tokenNames)
	}
}
