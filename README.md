Testproject for SO ["xtext: How to use unknown types for generics in the JVM inferrer?"](https://stackoverflow.com/questions/48266426/xtext-how-to-use-unknown-types-for-generics-in-the-jvm-inferrer)

Notes:

* the project uses Gradle composite build
  * the `unknown_type_usage` project depends on `unknown_types_dsl.parent`
  * this is configured in `unknown_type_usage/settings.gradle`
* the ci build is expected to fail
* we also generate an xml file that can be used for IntelliJ syntax highlighting
  * i.e. copy it to the IntelliJ config folder and restart he IDE
  * e.g. `%USERPROFILE%\config\settingsRepository\repository\filetypes\`
  * see also: [Directories used by the IDE to store settings, caches, plugins and logs](https://intellij-support.jetbrains.com/hc/en-us/articles/206544519)


  

